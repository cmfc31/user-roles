
# User Roles Management

**Developer:** Martin Fuentes 🚀


## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```


## Tech Stack 
This project is made with latest Node LTS version (v14.16.0). If you need to 
handle multple node.js versions on your machine I recommend to use Node Version 
Manager tool like: [nvm-windows](https://github.com/coreybutler/nvm-windows).

- Vue.js v2.6.x
- Vuetify v2.6.3
- Vue-Router
- Vuex

#### Integrated Linter
Default ESLint rules configured in dedicated config files.
- ESLint + Prettier

#### Recommend VS Code extensions

- [Vetur](https://marketplace.visualstudio.com/items?itemName=octref.vetur)
- [Vue VSCode Snippets](https://marketplace.visualstudio.com/items?itemName=sdras.vue-vscode-snippets)
- [ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)

#### Screenshots
![Preview 1](https://gitlab.com/cmfc31/user-roles/-/raw/master/public/asstes/preview1.jpg "Preview 1")
![Preview 2](https://gitlab.com/cmfc31/user-roles/-/raw/master/public/asstes/preview2.jpg "Preview 2")
![Preview 3](https://gitlab.com/cmfc31/user-roles/-/raw/master/public/asstes/preview3.jpg "Preview 3")
