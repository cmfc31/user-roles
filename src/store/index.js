import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    cmbUsers: [
      {
        id: 1,
        first_name: 'Jason',
        last_name: 'Mossburg',
        photo_url: 'http://placekitten.com/60/60'
      },
      {
        id: 2,
        first_name: 'Bill',
        last_name: 'Hayes',
        photo_url: 'http://placekitten.com/60/60'
      },
      {
        id: 3,
        first_name: 'Nathan',
        last_name: 'Norman',
        photo_url: 'http://placekitten.com/60/60'
      },
      {
        id: 4,
        first_name: 'Ron',
        last_name: 'Mitchell',
        photo_url: 'http://placekitten.com/60/60'
      },
      {
        id: 5,
        first_name: 'Doug',
        last_name: 'Tutt',
        photo_url: 'http://placekitten.com/60/60'
      },
      {
        id: 6,
        first_name: 'Sarah',
        last_name: 'Jane',
        photo_url: 'http://placekitten.com/60/60'
      },
      {
        id: 7,
        first_name: 'Adrian',
        last_name: 'Davis',
        photo_url: 'http://placekitten.com/60/60'
      },
      {
        id: 8,
        first_name: 'Conrad',
        last_name: 'Nguyen',
        photo_url: 'http://placekitten.com/60/60'
      },
      {
        id: 9,
        first_name: 'Kevin',
        last_name: 'McSweeney',
        photo_url: 'http://placekitten.com/60/60'
      }
    ],
    cmbRoleTypes: [
      {
        type: 'admin',
        description: 'Administrator Role'
      },
      {
        type: 'job_admin',
        description: 'Job Administrator'
      }
    ],
    cmbRoleStatus: [
      {
        id: 1,
        description: 'Active and Inactive'
      },
      {
        id: 2,
        description: 'Active'
      },
      {
        id: 3,
        description: 'Inactive'
      }
    ],
    userRoles: [
      {
        id: 1,
        name: 'Super Admin',
        type: 'admin',
        description:
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat',
        editable: false,
        active: true,
        users: [
          {
            id: 1,
            first_name: 'Jason',
            last_name: 'Mossburg',
            photo_url: 'http://placekitten.com/60/60'
          },
          {
            id: 2,
            first_name: 'Bill',
            last_name: 'Hayes',
            photo_url: 'http://placekitten.com/60/60'
          }
        ],
        created_on: '2019-01-18T18:25:43.511Z',
        modified_on: '2019-01-18T18:25:43.511Z'
      },
      {
        id: 2,
        name: 'Company Admin',
        type: 'admin',
        description:
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat',
        editable: false,
        active: true,
        users: [
          {
            id: 5,
            first_name: 'Doug',
            last_name: 'Tutt',
            photo_url: 'http://placekitten.com/60/60'
          }
        ],
        created_on: '2019-01-18T18:25:43.511Z',
        modified_on: '2019-01-18T18:25:43.511Z'
      },
      {
        id: 3,
        name: 'Property Admin',
        type: 'admin',
        description:
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat',
        editable: false,
        active: false,
        users: [
          {
            id: 6,
            first_name: 'Sarah',
            last_name: 'Jane',
            photo_url: 'http://placekitten.com/60/60'
          },
          {
            id: 7,
            first_name: 'Adrian',
            last_name: 'Davis',
            photo_url: 'http://placekitten.com/60/60'
          },
          {
            id: 8,
            first_name: 'Conrad',
            last_name: 'Nguyen',
            photo_url: 'http://placekitten.com/60/60'
          },
          {
            id: 9,
            first_name: 'Kevin',
            last_name: 'McSweeney',
            photo_url: 'http://placekitten.com/60/60'
          }
        ],
        created_on: '2019-01-18T18:25:43.511Z',
        modified_on: '2019-01-18T18:25:43.511Z'
      },
      {
        id: 4,
        name: 'Job Recruiter',
        type: 'job_admin',
        description:
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat',
        editable: true,
        active: true,
        users: [
          {
            id: 2,
            first_name: 'Bill',
            last_name: 'Hayes',
            photo_url: 'http://placekitten.com/60/60'
          },
          {
            id: 4,
            first_name: 'Ron',
            last_name: 'Mitchell',
            photo_url: 'http://placekitten.com/60/60'
          },
          {
            id: 6,
            first_name: 'Sarah',
            last_name: 'Jane',
            photo_url: 'http://placekitten.com/60/60'
          }
        ],
        created_on: '2019-01-18T18:25:43.511Z',
        modified_on: '2019-01-18T18:25:43.511Z'
      },
      {
        id: 5,
        name: 'Content Administrator',
        type: 'admin',
        description:
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat',
        editable: true,
        active: true,
        users: [
          {
            id: 1,
            first_name: 'Jason',
            last_name: 'Mossburg',
            photo_url: 'http://placekitten.com/60/60'
          },
          {
            id: 3,
            first_name: 'Nathan',
            last_name: 'Norman',
            photo_url: 'http://placekitten.com/60/60'
          },
          {
            id: 5,
            first_name: 'Doug',
            last_name: 'Tutt',
            photo_url: 'http://placekitten.com/60/60'
          }
        ],
        created_on: '2019-01-18T18:25:43.511Z',
        modified_on: '2019-01-18T18:25:43.511Z'
      },
      {
        id: 6,
        name: 'Property Moderator',
        type: 'admin',
        description:
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat',
        editable: false,
        active: true,
        users: [
          {
            id: 7,
            first_name: 'Adrian',
            last_name: 'Davis',
            photo_url: 'http://placekitten.com/60/60'
          },
          {
            id: 8,
            first_name: 'Conrad',
            last_name: 'Nguyen',
            photo_url: 'http://placekitten.com/60/60'
          },
          {
            id: 9,
            first_name: 'Kevin',
            last_name: 'McSweeney',
            photo_url: 'http://placekitten.com/60/60'
          }
        ],
        created_on: '2019-01-18T18:25:43.511Z',
        modified_on: '2019-01-18T18:25:43.511Z'
      }
    ]
  },
  getters: {
    getUserRoleById: (state) => (id) => {
      return state.userRoles.find((obj) => {
        return obj.id == id
      })
    },
    userRolesFiltered: (state) => (search, status) => {
      return state.userRoles
        .filter((obj) => {
          // Filter by search string in role name
          return !search || new RegExp(search, 'i').test(obj.name)
        })
        .filter((obj) => {
          // Filter by role status
          return status == 1 || (status == 2 ? obj.active : !obj.active)
        })
    }
  },
  mutations: {
    ADD_ROLE(state, roleData) {
      // Get current last id + 1
      let newId =
        state.userRoles.map((obj) => {
          return obj.id
        })[state.userRoles.length - 1] + 1
      roleData.id = newId
      state.userRoles.push(roleData)
    },
    UPDATE_ROLE(state, roleData) {
      let index = state.userRoles
        .map((obj) => {
          return obj.id
        })
        .indexOf(Number(roleData.id))
      state.userRoles[index] = roleData
    },
    DELETE_ROLE(state, roleId) {
      let index = state.userRoles
        .map((obj) => {
          return obj.id
        })
        .indexOf(roleId)

      state.userRoles.splice(index, 1)
    }
  },
  actions: {
    addRole({ commit }, roleData) {
      commit('ADD_ROLE', roleData)
    },
    updateRole({ commit }, roleData) {
      commit('UPDATE_ROLE', roleData)
    },
    deleteRole({ commit }, roleId) {
      commit('DELETE_ROLE', roleId)
    }
  },
  modules: {}
})
