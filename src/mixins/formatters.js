export default {
  methods: {
    // ----------------- DATE FORMATING ------------------------
    /* Functions for date formatting */

    // Converts yyyy-MM-dd to dd/MM/yyyy
    formatDate(date) {
      if (!date) return null
      const [year, month, day] = date.split('-')
      return `${day}/${month}/${year}`
    },
    // Converts dd/MM/yyyy to yyyy-MM-dd
    parseDate(date) {
      if (!date) return null
      const [day, month, year] = date.split('/')
      return `${year}-${month.padStart(2, '0')}-${day.padStart(2, '0')}`
    },
    // Converts yyyy-MM-dd to MM/dd/yyyy
    formatDateMonthFirst(date) {
      if (!date) return null
      const [year, month, day] = date.split('-')
      return `${month}/${day}/${year}`
    },
    // Converts MM/dd/yyyy to yyyy-MM-dd
    parseDateMonthFirst(date) {
      if (!date) return null
      const [month, day, year] = date.split('/')
      return `${year}-${month}-${day}`
    },
    // Converts yyyy-MM to yyyy/MM
    formatYearMonth(date) {
      if (!date) return null
      const [year, month] = date.split('-')
      return `${year}/${month}`
    },
    // Convierte yyyy/MM a yyyy-MM
    parseYearMonth(date) {
      if (!date) return null
      const [year, month] = date.split('/')
      return `${year}-${month.padStart(2, '0')}`
    },
    // Returns date in ISO format with local timezone
    /* The main function toISOString() return date in UTC */
    toISOLocal(d) {
      var z = (n) => (n < 10 ? '0' : '') + n
      var off = d.getTimezoneOffset()
      var sign = off < 0 ? '+' : '-'
      off = Math.abs(off)

      return (
        d.getFullYear() +
        '-' +
        z(d.getMonth() + 1) +
        '-' +
        z(d.getDate()) +
        'T' +
        z(d.getHours()) +
        ':' +
        z(d.getMinutes()) +
        ':' +
        z(d.getSeconds()) +
        sign +
        z((off / 60) | 0) +
        z(off % 60)
      )
    }
  }
}
